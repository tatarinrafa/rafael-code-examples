#pragma once

// Our App Core

class CLog;

class CCore
{
public:
	CCore();
	virtual ~CCore();

	void				Initialize			();
	void				Destroy				();

	inline u32			GetCPUThreadsCount	() { return std::thread::hardware_concurrency(); };

	inline CLog*		GetLog				() { return m_Log; };

private:

	CLog* m_Log;
};