#include "stdafx.h"
#include "Log.h"
#include <stdarg.h>

CLog::CLog()
{
}

CLog::~CLog()
{
}

void CLog::Initialize()
{
	Log(_T("Log Initialized"));
}

void CLog::Destroy()
{
	Log(_T("Log Destroying"));
}

void CLog::LogMsgImplementetion(LPCString msg)
{
	// Support for MT calling
	m_AccessLock.lock();

	if (msg)
#ifdef UNICODE
		std::wcout << msg << '\n';
#else
		std::cout << msg << '\n';
#endif

	m_AccessLock.unlock();
}

void Log(LPCString format, ...)
{
	// Sanity check in case of a call when log is not initialized
	if (!Application || !Application->GetCore() || !Application->GetCore()->GetLog())
		return;

#ifdef UNICODE
	wchar_t formated_msg[512];
	va_list args;
	va_start(args, format);
	vswprintf(formated_msg, sizeof(formated_msg), format, args);
	va_end(args);
#else
	char formated_msg[512];
	va_list args;
	va_start(args, format);
	vsprintf_s(formated_msg, sizeof(formated_msg), format, args);
	va_end(args);
#endif

	Application->GetCore()->GetLog()->LogMsgImplementetion(formated_msg);
}