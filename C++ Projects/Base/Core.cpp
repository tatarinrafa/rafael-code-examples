#include "stdafx.h"
#include "Core.h"

CCore::CCore()
{
	m_Log = nullptr;
}

CCore::~CCore()
{
}

void CCore::Initialize()
{
	m_Log = new CLog();

	m_Log->Initialize();

	Log(_T("Core Initialized"));
}

void CCore::Destroy()
{
	Log(_T("Core Destroying"));

	m_Log->Destroy();

	delete m_Log;
}
