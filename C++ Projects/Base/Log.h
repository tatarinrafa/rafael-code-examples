#pragma once

class CLog
{
public:
	CLog();
	virtual ~CLog();

	void				Initialize				();
	void				Destroy					();

	void				LogMsgImplementetion	(LPCString msg);

private:

	std::mutex m_AccessLock;
};

// essential logging wraper function for the app. Declared globaly for handy access
extern void Log(LPCString format, ...);
