#include "stdafx.h"
#include "ApplicationBase.h"

CApplicationBase* Application = nullptr;

CApplicationBase::CApplicationBase()
{
	m_Core = nullptr;
}

CApplicationBase::~CApplicationBase()
{
}

void CApplicationBase::Initialize()
{
	m_Core = new CCore();
	m_Core->Initialize();
}

void CApplicationBase::Destroy()
{
	m_Core->Destroy();
	delete m_Core;
}
