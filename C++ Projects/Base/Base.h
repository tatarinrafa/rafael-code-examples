#pragma once

#ifdef UNICODE
typedef std::wstring String;
typedef _Null_terminated_ const wchar_t* LPCString;
#define _T(x) L ## x

typedef std::wifstream InPutFile;
typedef std::wofstream OutPutFile;
#else
typedef std::string String;
typedef _Null_terminated_ const char* LPCString;
#define _T(x) x

typedef std::ifstream InPutFile;
typedef std::ofstream OutPutFile;
#endif

typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned __int64 u64;

// Include essential headers
#include "Core.h"
#include "Log.h"
#include "ApplicationBase.h"