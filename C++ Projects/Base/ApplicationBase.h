#pragma once

class CApplicationBase
{
public:

	CApplicationBase();
	virtual ~CApplicationBase();

	virtual void		Initialize				();
	virtual void		Destroy					();

	virtual void		MainProcessing			() = 0;

	inline CCore*		GetCore					() { return m_Core; };

private:

	CCore* m_Core;
};

// Essential object. Declared globaly for handy access
extern CApplicationBase* Application;