#pragma once

class CStringParser
{
public:
	enum EParserErrorType
	{
		eOk,
		eNoFileName,
		eNoInputFile,
		eNoOutputFile,
		eFileIsEmpty,
		eUnknown,
	};

	CStringParser();
	virtual ~CStringParser();

	void				Initialize				();
	void				Destroy					();

	EParserErrorType	ParseFile				(LPCString input_file_name, LPCString output_file_name);
	
	_NODISCARD String	TranslateErrorCode		(CStringParser::EParserErrorType error_type);

private:

	struct SThreadParams
	{
		SThreadParams(size_t start, size_t end, std::vector<String*>* input_strings, std::map<size_t, String*>* output_strings_map)
		{
			m_StartStringIndex = start;
			m_EndStringIndex = end;

			m_pInputStrings = input_strings;
			m_pOutputStringsMap = output_strings_map;

			// Allocate in heap to not overflow stack. Thread must perform memory clean up when done
			m_pParsedStrings = new std::map<size_t, String*>();
		}

		size_t m_StartStringIndex;
		size_t m_EndStringIndex;
		std::vector<String*>* m_pInputStrings;
		std::map<size_t, String*>* m_pOutputStringsMap;

		std::map<size_t, String*>* m_pParsedStrings;
	};

private:

	bool				ReadInput				(InPutFile& input_file);
	void				Parse					();
	bool				WriteOutput				(OutPutFile& output_file);
	static void			ThreadImpl				(SThreadParams thread_params);

private:

	std::vector<String*> m_InputStrings;
	std::map<size_t, String*> m_OutputStringsMap;

	std::vector<std::thread> m_Threads;
};