#include "stdafx.h"
#include "ParserApplication.h"
#include "Core.h"
#include "StringParser.h"

CParserApplication::CParserApplication()
{
	m_StringParser = nullptr;
}

CParserApplication::~CParserApplication()
{
}

void CParserApplication::Initialize()
{
	inherited::Initialize();

	m_StringParser = new CStringParser();
	m_StringParser->Initialize();

	Log(_T("Application Initialized"));
}

void CParserApplication::Destroy()
{
	Log(_T("Application Destroying"));

	m_StringParser->Destroy();
	delete m_StringParser;

	inherited::Destroy();
}

void CParserApplication::MainProcessing()
{
	String input_file_name = _T("input.txt");
	String output_file_name = _T("output.txt");

	Log(_T("Input File Name: %s"), input_file_name.c_str());
	Log(_T("Output File Name: %s"), output_file_name.c_str());

	// Parse the file
	CStringParser::EParserErrorType res = m_StringParser->ParseFile(input_file_name.c_str(), output_file_name.c_str());

	// Check if parser has encountered an error 
	// And print error description in output
	if (res)
	{
		String msg = m_StringParser->TranslateErrorCode(res);

		Log(_T("Could not process file due to: %s \n"), msg.c_str());
	}
	else
	{
		Log(_T("Successfully Completed"));
	}
}
