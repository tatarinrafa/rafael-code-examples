#pragma once

#include "ApplicationBase.h"

class CStringParser;
class CCore;

class CParserApplication : public CApplicationBase
{
	typedef CApplicationBase inherited;

public:

	CParserApplication();
	virtual ~CParserApplication();

	void				Initialize				() override;
	void				Destroy					() override;
	void				MainProcessing			() override;

private:

	CStringParser* m_StringParser;
};
