#include "stdafx.h"

int main()
{
	// Init app instance

	Application = new CParserApplication();

	Application->Initialize();

	Application->MainProcessing();

	Application->Destroy();

	delete Application;

	return 0;
}
