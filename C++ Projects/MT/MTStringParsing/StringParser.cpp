#include "stdafx.h"
#include "StringParser.h"

using namespace std;

CStringParser::CStringParser()
{
}

CStringParser::~CStringParser()
{
}

void CStringParser::Initialize()
{
	Log(_T("String Parser Initialized"));
}

void CStringParser::Destroy()
{
	Log(_T("String Parser Destroyed"));
}

CStringParser::EParserErrorType CStringParser::ParseFile(LPCString input_file_name, LPCString output_file_name)
{
	if (!input_file_name || !output_file_name)
		return eNoFileName;

	// Open output file in advance to make sure we have an access to it before we start parsing
	// So that we don't uselessly perform our job and realize that output file is blocked only at the finish line
	OutPutFile output_file(output_file_name);

	if (!output_file.is_open())
	{
		return eNoOutputFile;
	}

	InPutFile input_file(input_file_name);

	if (ReadInput(input_file))
	{
		if (!m_InputStrings.size())
			return eFileIsEmpty;

		Parse();

		if (WriteOutput(output_file))
		{
			return eOk; // The only right way out =)
		}
		else
		{
			return eNoOutputFile;
		}
	}
	else
	{
		return eNoInputFile;
	}

	return eUnknown;
}

String CStringParser::TranslateErrorCode(CStringParser::EParserErrorType error_type)
{
	String msg;

	switch (error_type)
	{
	case EParserErrorType::eNoFileName:
	{
		msg = _T("Input or output file name not specified");
		break;
	}
	case EParserErrorType::eNoInputFile:
	{
		msg = _T("Input file was not found");
		break;
	}
	case EParserErrorType::eNoOutputFile:
	{
		msg = _T("Output file could not be created");
		break;
	}
	case EParserErrorType::eFileIsEmpty:
	{
		msg = _T("Input file does not contain a single string");
		break;
	}
	default:
	{
		msg = _T("Uknown Error");
		break;
	}
	}

	return msg;
}

bool CStringParser::ReadInput(InPutFile& input_file)
{	
	// Read strings and store them in a vector container

	Log(_T("Reading Input"));

	if (input_file.is_open())
	{
		String line;

		while (getline(input_file, line))
		{
			// Create string in the heap to minimize memory usage in further processing
			// Just operate over the same instance of a given string, avoiding copying
			m_InputStrings.emplace_back(new String(line));
		}

		input_file.close();

		return true;
	}

	return false;
}

void CStringParser::Parse()
{
	// Decide how many threads are available and split workload
	// Leave one thread untouched for system, if possible
	size_t threads_to_use = std::max(u32(1), Application->GetCore()->GetCPUThreadsCount() - 1);
	// In case workload size is less than threads count
	threads_to_use = std::min(m_InputStrings.size(), threads_to_use);

	size_t chunk_size = m_InputStrings.size() / threads_to_use;

	m_Threads.clear();

	Log(_T("Starting to parse with %d threads"), threads_to_use);

	// Start the parsing process in multiple threads
	for (u32 thread_indx = 0; thread_indx < threads_to_use; ++thread_indx)
	{
		size_t start = chunk_size * thread_indx;
		size_t end = start + chunk_size;

		m_Threads.emplace_back(std::thread(CStringParser::ThreadImpl, SThreadParams(start, end, &m_InputStrings, &m_OutputStringsMap)));
	}

	// Wait for threads to complete the task
	for (auto& thread : m_Threads)
		thread.join();

	m_Threads.clear();
}

bool CStringParser::WriteOutput(OutPutFile& output_file)
{
	// Write parsed strings to the output file

	Log(_T("Writing Output"));

	// Double check that we did not loose output file access for some reason
	if (output_file.is_open())
	{
		for (auto it : m_OutputStringsMap)
		{
			String& out = *(it.second);

			output_file << out.c_str();
			output_file << '\n';

			// Clean up our heap allocated string instance
			delete it.second;
		}

		output_file.close();

		m_InputStrings.clear();
		m_OutputStringsMap.clear();

		return true; // The only right way out =)
	}
	else
	{
		// Something went wrong. Clean up anyway
		for (auto it : m_OutputStringsMap)
		{
			// Clean up our heap allocated string instance
			delete it.second;
		}

		m_InputStrings.clear();
		m_OutputStringsMap.clear();

		return false;
	}

	return false;
}

void CStringParser::ThreadImpl(SThreadParams thread_params)
{
	// Perform our workload
	for (size_t index = thread_params.m_StartStringIndex; index < thread_params.m_EndStringIndex && index < thread_params.m_pInputStrings->size(); ++index)
	{
		String& in = *thread_params.m_pInputStrings->at(index);
		std::reverse(in.begin(), in.end());

		thread_params.m_pParsedStrings->emplace(std::pair{ index, thread_params.m_pInputStrings->at(index) });
	}

	// Store result in output map
	static mutex access_lock_write_to_output_map;

	access_lock_write_to_output_map.lock();

	thread_params.m_pOutputStringsMap->merge(*thread_params.m_pParsedStrings);

	access_lock_write_to_output_map.unlock();

	// Clean Up
	delete thread_params.m_pParsedStrings;
}
